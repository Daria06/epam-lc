﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {

            string ex = "yes";
            while (ex == "yes")
            {
                Console.WriteLine("Меню:\n");
                Console.WriteLine("1: Rectangle\n" +
                                  "2: Triangle\n" +
                                  "3: AnotherTriangle\n" +
                                  "4: XmasTree\n" +
                                  "5: Выйти");
                
                string value = Console.ReadLine();
                int menuitem;
                bool success = Int32.TryParse(value, out menuitem);

                if (success)
                {
                    switch (menuitem)
                    {
                        case 1:
                            Console.WriteLine("Введите числа A и B для выбранной функции:\n");
                            string valueA = Console.ReadLine();
                            string valueB = Console.ReadLine();
                            int numberA;
                            int numberB;
                            bool successA;
                            bool successB;
                            successA = Int32.TryParse(valueA, out numberA);
                            successB = Int32.TryParse(valueB, out numberB);

                            if (successA && successB)
                            {
                                Console.WriteLine("Результат:");
                                task1.Rectangle(numberA, numberB);
                            }
                            else
                            {
                                Console.WriteLine("Вы ввели не число ");
                            }
                            break;
                        case 2:
                            Console.WriteLine("Введите число для выбранной функции:\n");
                            string value2 = Console.ReadLine();
                            int number;
                            bool success2 = Int32.TryParse(value2, out number);
                            if (success2)
                            {
                                Console.WriteLine("Результат:");
                                task1.Triangle(number);
                            }
                            else
                            {
                                Console.WriteLine("Вы ввели не число ");
                            }
                            break;
                        case 3:
                            Console.WriteLine("Введите число для выбранной функции:\n");
                            string value3 = Console.ReadLine();
                            int number2;
                            bool success3 = Int32.TryParse(value3, out number2);
                            if (success3)
                            {
                                Console.WriteLine("Результат:");
                                task1.AnotherTriangle(number2);
                            }
                            else
                            {
                                Console.WriteLine("Вы ввели не число ");
                            }
                            break;
                        case 4:
                            Console.WriteLine("Введите число для выбранной функции:\n");
                            string value4 = Console.ReadLine();
                            int number4;
                            bool success4 = Int32.TryParse(value4, out number4);
                            if (success4)
                            {
                                Console.WriteLine("Результат:");
                                task1.XmasTree(number4);
                            }
                            else
                            {
                                Console.WriteLine("Вы ввели не число ");
                            }
                            break;
                        case 5:
                            Console.WriteLine("Вы выбрали выйти\n");
                            ex = "no";
                            break;
                        default:
                            Console.WriteLine("Выберите существующий пункт меню");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Вы ввели не число ");
                }

            }
        }
    }
}
   
public static class task1
{
    public static void Rectangle(int a, int b)
    {
        if (a > 0 && b > 0)
        {
            int res = a * b;
            Console.WriteLine(res);
        }
        else
        {
            Console.WriteLine("Вы ввели число не больше нуля");
        }
    }
    public static void Triangle(int n)
    {
        if (n >= 0)
        {
            for (int i = 0; i < n+1; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine("\n");
            }
        }
        else
        {
            Console.WriteLine("Вы ввели отрицательное число");
        }

    }
    public static void AnotherTriangle(int n)
    {
        if (n >= 0)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = n-1; j > i; j--)
                {
                    Console.Write("  ");
                }
                for (int j = 0; j < i; j++)
                {
                    Console.Write("* ");
                }
                for (int j = 1; j < i; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine("\n");
            }
        }
        else
        {
            Console.WriteLine("Вы ввели отрицательное число");
        }
    }
    public static void XmasTree(int n)
    {
        if (n >= 0)
        {
            for (int x = 0; x < n; x++) {
                for (int i = 0; i < x; i++)
                {
                    for (int j = n - 1; j > i; j--)
                    {
                        Console.Write("  ");
                    }
                    for (int j = 0; j < i; j++)
                    {
                        Console.Write("* ");
                    }
                    for (int j = 1; j < i; j++)
                    {
                        Console.Write("* ");
                    }
                    Console.WriteLine(" ");
                }
            }
        }
        else
        {
            Console.WriteLine("Вы ввели отрицательное число");
        }

    }
    public static bool Simple(int n)
    {
        bool res = true;
        for (int i = 2; i <= n / 2; i++)
        {
            if (n % i == 0)
            {
                res = false;
                break;
            }
        }
        return res;

        /*
        Простое число делится на 1 и само на себя. 
        Максимальный делитель, у любого числа,
        когда остаток от деления может быть равен 0 - это половина этого числа.
        Вот и проверяем от 2 и до n / 2, 
        если есть в этом промежутке делитель про котором остаток равен 0, то число не простое
        */
    }

}
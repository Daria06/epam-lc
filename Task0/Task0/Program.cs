﻿using System;

namespace Task0
{
    class Program
    {
        static void Main(string[] args)
        {
            string ex = "yes";
            while (ex == "yes")
            {
                Console.WriteLine("Меню:\n");
                Console.WriteLine("1: Sequence\n" +
                                  "2: Simple\n" +
                                  "3: Square\n" +
                                  "4: Выйти\n");
                string value = Console.ReadLine();
                int menuitem;
                bool success = Int32.TryParse(value, out menuitem);


                string value2;
                int  number;
                bool success2;
                
                if (success)
                {
                    switch (menuitem)
                    {
                    case 1:
                            {
                                Console.WriteLine("Введите число для выбранной функции:\n");
                                value2 = Console.ReadLine();
                                success2 = Int32.TryParse(value2, out number);
                                try
                                {
                                    if (success2) { Console.WriteLine("Результат:"); int[] res = Task0.Sequence(number); foreach (int i in res) { Console.WriteLine(i); } }
                                    else { Console.WriteLine("Вы ввели не число "); }
                                    break;
                                }
                                catch (Exception e) {
                                    Console.WriteLine(e.Message);
                                    break;
                                }                               
                            }
                    case 2:
                            {
                                Console.WriteLine("Введите число для выбранной функции:\n");
                                value2 = Console.ReadLine();
                                success2 = Int32.TryParse(value2, out number);
                                try
                                {
                                    if (success2)
                                    {
                                        Console.WriteLine("Результат:");
                                        if (Task0.Simple(number)) { Console.WriteLine("Введенное число - простое"); }
                                        else { Console.WriteLine("Введенное число - не является простым"); };
                                    }
                                    else { Console.WriteLine("Вы ввели не число "); }
                                    break;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                    break;
                                }
                            }
                    case 3:
                            {
                                Console.WriteLine("Введите число для выбранной функции:\n");
                                value2 = Console.ReadLine();
                                success2 = Int32.TryParse(value2, out number);
                                if (success2) { Console.WriteLine("Результат:"); Task0.Square(number); }
                                else { Console.WriteLine("Вы ввели не число "); }
                                break;
                            }
                    case 4:     Console.WriteLine("Вы выбрали выйти\n"); ex = "no";break;
                    default:    Console.WriteLine("Выберите существующий пункт меню");break;
                    }
                }
                else {Console.WriteLine("Вы ввели не число ");}                           
            }
        }
    }
    public static class Task0
    {
        public static int[] Sequence(int n)
        {
            if (n > 0)
            {
                int[] res = new int[n];
                for (int i = 0; i < n; i++)
                {
                    res[i] = i + 1;
                }
                return res;
            }
            else { throw new Exception("Введённое значение не больше нуля"); }
            
        }

        public static bool Simple(int n)
        {
            bool res = true;
            for (int i = 2; i <= n / 2; i++)
            {
                if (n % i == 0)
                {
                    res = false;
                    break;
                }
            }
            return res;
            /*
            Простое число делится на 1 и само на себя. 
            Максимальный делитель, у любого числа,
            когда остаток от деления может быть равен 0 - это половина этого числа.
            Вот и проверяем от 2 и до n / 2, 
            если есть в этом промежутке делитель про котором остаток равен 0, то число не простое
            */
        }
        public static void Square(int n)
        {
            if (n >= 0)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        Console.Write("* ");
                    }
                    Console.WriteLine("\n");
                }
            } else { throw new Exception("Вы ввели отрицательное число"); }
        }
    }
}
